import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  //SidebarFooter,
  SidebarContent
} from "react-pro-sidebar";
//import "react-pro-sidebar/dist/css/styles.css";
import MenuPage from "../menu";
import "../../pages/style.css";
import FullCard from "../Cards/fullCard";
import UserProfilePage from "../../pages/profile";
//var view = false;
class NavBar extends Component {
  ui = {
    menu: {
      text: "Меню"
    },
    catalog: {
      text: "Каталог"
    }
  };
  checkAlias(text) {
    //console.log( "aliases:\t" + this.props.aliases)
    //return text;
    if (text in this.props.aliases) {
      return this.props.aliases[text];
      //console.log(this.props.aliases[text]);
    } else {
      return text;
      //console.log(e);
    }
  }
  viewPakegeInfo(pkgName) {
    const url =  this.props.ServerAdress + "/find?pkg=" + pkgName;
    console.log(url);
    const response = fetch(url);
    /*try {
      portage_list[pkgName];
    } catch (e) {*/

    if (response.ok) {
      // если HTTP-статус в диапазоне 200-299
      // получаем тело ответа (см. про этот метод ниже)
      let res = response.json();
      ReactDOM.render(
        <FullCard
          PackageName={res}
          PropMain={this.props.rec}
          icons={this.props.icons}
        />,
        document.getElementById("HomePage")
      );
      document.getElementById("inS").value = "";
    } else {
      alert("Ошибка HTTP: " + response.status);
      //}
    }

    console.log(pkgName);
  }
  findPakg(pkg) {
    this.viewPakegeInfo(pkg);
  }
  render() {
    return (
      <ProSidebar
        id="navBar"
        style={{
          background: "2e323f",
          position: "fixed",
          height: "100vh",
          left: "-280px"
        }}
      >
        <SidebarHeader>
          <div
            style={{
              padding: "24px",
              textTransform: "uppercase",
              fontWeight: "bold",
              fontSize: "14px",
              letterSpacing: "1px",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              paddingLeft: "12px"
            }}
          >
            Webport
          </div>

          <MenuItem>
            <div
              className="user-pic"
              style={{
                width: "60px",
                marginRight: "3px",
                float: "left",
                paddingLeft: "12px"
              }}
            >
              <img
                style={{ width: "56px", height: "56px" }}
                className="img-responsive img-rounded"
                src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
                alt="User picture"
                onClick={() => (
                  (document.getElementById("backBtn").style.display = "block"),
                  //(document.getElementById("container").style.display = "none"),
                  //(document.getElementById("conteinerInfo").style.display =
                  //  "block"),
                  ReactDOM.render(
                    <UserProfilePage
                      Ipkgs={this.props.Ipkgs}
                      catalog={this.props.category}
                    />,
                    document.getElementById("HomePage")
                  )
                )}
              />
            </div>
            <div
              className="user-info"
              style={{ float: "left", width: " 100%" }}
            >
              <span className="user-name">
                Jhon
                <strong>Smith</strong>
              </span>
              <span
                className="user-role"
                style={{ width: "100vw", color: "green" }}
              >
                Guest
              </span>
              {/* <span className="user-status">
                <i className="fa fa-circle"></i>
                <span>Online</span>
          </span>*/}
            </div>
          </MenuItem>
          <MenuItem style={{}}>
            {/* <input
              id="inS"
              name="inS"
              classname="form-control"
              type="text"
              placeholder="Введите имя пакета"
              onChange={(e) => this.findPakg(e.target.value)}
              style={{
                fontSize: "1.2em",
                margin: "10px",
                backgroundColor: "rgba(81, 81, 81, 0.5)",
                border: "none",
                padding: "5px",
                //ackground: "#2e233f",
                //borderRadius: "15px",
                //border: "3px inset #2e322f",
                color: "#c7c7c7"
              }}
            />*/}
          </MenuItem>
        </SidebarHeader>
        <SidebarContent style={{ overflowX: "clip" }}>
          <Menu>
            <MenuPage
              menuItem={this.props.menuItem}
              title={this.ui.menu.text}
            />
            <MenuItem
              title={this.ui.catalog.text}
              style={{ paddingRight: "0px" }}
            >
              {/*CATALOG */}
              <SubMenu title={this.ui.catalog.text}>
                {Object.keys(this.props.category).map((k, i) => (
                  <MenuItem
                    style={{
                      paddingRight: "0px",
                      width: "100vw"
                    }}
                  >
                    <SubMenu title={this.checkAlias(k)}>
                      {this.props.category[k].map((pn, j) => (
                        <MenuItem
                          onClick={() =>
                            this.viewPakegeInfo(
                              this.props.category[k][i] +
                                "/" +
                                this.props.category[k][j]
                            )
                          }
                          style={{
                            //borderBottom: "1px solid red ",
                            width: "100%"
                          }}
                        >
                          {pn}
                        </MenuItem>
                      ))}
                    </SubMenu>
                  </MenuItem>
                ))}
              </SubMenu>
            </MenuItem>
          </Menu>
        </SidebarContent>
      </ProSidebar>
    );
  }
}
export default NavBar;
