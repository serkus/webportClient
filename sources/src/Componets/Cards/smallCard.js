import React from "react";
import ReactDOM from "react-dom";
import FullCard from "./fullCard";
//import PkgList from "../../pages/pks";
import "./style.css";
//import Swiper from "react-slider-swiper";
//import InstallForm from "../pages/InstallForm";
const ui = { description: "Описание" };
class Scard extends React.Component {
  //Viewinstall(package) {
  //const pkgInfo = this.props.rec[package]
  //return <InstallForm rec={pkgInfo} />;
  //}:
  params = {
    pagination: ".swiper-pagination",
    paginationClickable: true,
    nextButton: ".swiper-button-next",
    prevButton: ".swiper-button-prev",
    spaceBetween: 30,
    runCallbacksOnInit: true,
    onInit: (swiper) => {
      this.swiper = swiper;
    }
  };

  viweinfo(pkg) {
    //onClick={() => this.props.ViewPackageInfo(this.props.rec[k][i])}
    document.getElementById("backBtn").style.display = "block";
    document.getElementById("pTitle").textContent = pkg.Name.split("/")[1];
    document.getElementById("container").style.display = "none";

    document.getElementById("containerInfo").style.display = "block";
    ReactDOM.render(
      <FullCard
        PackageName={pkg}
        PropMain={this.props.rec}
        icons={this.props.icons}
        useDes={this.props.useDes}
        Ipkg={this.props.Ipkg}
      />,
      document.getElementById("containerInfo")
    );
  }

  render() {
    return (
      <div
        className="card__list"
        style={{
          padding: "5px",
          //margin: "3px",
          marginTop: "10px",
          //width: "100vw",
          display: "inline-flex",
          border: "1px solid #CCC",
          borderRadius: " 10px",
          boxShadow: "3px 4px 10px #CDC"
          //height:"75px"
        }}
        //onClick={() => this.viweinfo(this.props.rec[k][i])}
      >
        <img
          className="card__img"
          style={{
            width: "64px",
            height: "64px"
          }}
          src={this.props.icons[this.props.pn.Name.split("/")[1]]}
          alt={this.props.pn.Name.split("/")[1]}
        />
        <div style={{ marginLeft: "10px" }}>
          <b>
            {" "}
            <span
              className="card__title"
              style={{
                color: "red",
                width: "100vw",
                overflow: "clip",
                fontSize: "1.1em"
              }}
              key={this.props.pn.Name}
            >
              {this.props.pn.Name.split("/")[1]}
            </span>
          </b>
          <div style={{ overflow: "hidden" }}>{this.props.pn.Description}</div>
        </div>
      </div>
    );
  }
}
export default Scard;
