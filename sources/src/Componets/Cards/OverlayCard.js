import "./style.css";
import React from "react";
export default class OverlayCard extends React.Component {
  toBack() {
    //document.getElementById("pTitle").textContent = "Оверлеи";
    document.getElementById("containerInfo").style.display = "none";
  }
  render() {
    return (
      <div className="sCard" style={{}}>
        <h3 style={{}}>
          <span
            className="btn"
            id="backBtn"
            style={{
              padding: "0px 20px 0px 20px",
              float: "left",
              color: "#FFF",
              background: "#F00",
              marginRight: "0.85em",
              fontSize: "1.1em",
              borderRadius: "0.9em"

              //border: "1px outset #444",
              //background: "rgb(78,116,26)",
              // background: "rgb(78,116,26)",
              //background:
              // "linear-gradient(0deg, rgba(78,116,26,0.5536415249693627) 0%, rgba(229,237,235,0.9093838218881303) 35%)"
            }}
            onClick={() => this.toBack()}
          >
            <b style={{ float: "right" }}> {"X"}</b>
          </span>
          <b> {this.props.title.name}:</b> {this.props.OverlayInfo.name}
        </h3>
        <div>
          <p style={{ fontSize: "1.4em" }}>
            <b>{this.props.title.discription}:</b>
            {this.props.OverlayInfo.description}
          </p>
          <iframe
            style={{ width: "100%", height: " 100vh" }}
            src={this.props.OverlayInfo.homepage}
            seamless="false"
          ></iframe>
        </div>
      </div>
    );
  }
}
